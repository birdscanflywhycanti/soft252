package Week1.Example1.src.example1;

public class MyWeight
 {
    public static void main(String[] args) throws Exception {
        System.out.println("Input weight in kg:");
        int kg = 1;
        double pounds = kg *2.2;
        System.out.println(kg + " kg = " + pounds + " lb");
    }
}